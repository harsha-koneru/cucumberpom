$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/harsha.koneru/Desktop/Cucumber/cucumberpom/CucumberPOM/src/test/java/com/automation/features/LinkedIn.feature");
formatter.feature({
  "line": 1,
  "name": "LinkedIn Application Test",
  "description": "",
  "id": "linkedin-application-test",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Validate HomePage Test",
  "description": "",
  "id": "linkedin-application-test;validate-homepage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "user opens browser",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user is on linkedin page",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "user clicks on signIn button",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on login page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user logs into application",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "validate home page title",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user clicks MyNetwork Icon",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_opens_browser()"
});
formatter.result({
  "duration": 31137802000,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_is_on_linkedin_page()"
});
formatter.result({
  "duration": 52971500,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_clicks_on_signIn_button()"
});
formatter.result({
  "duration": 662233000,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_is_on_login_page()"
});
formatter.result({
  "duration": 1188267900,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_logs_into_applicationn()"
});
formatter.result({
  "duration": 6790735300,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.validate_home_page_title()"
});
formatter.result({
  "duration": 12877700,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.user_clicks_MyNetwork_Icon()"
});
formatter.result({
  "duration": 105002600,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Validate MyNetworkPage Test",
  "description": "",
  "id": "linkedin-application-test;validate-mynetworkpage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "user is on MyNetwork page",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "user clicks on more invitations",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "user clicks on see all connections",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "user enters name on search text box",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "user clicks message button",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "user enters text on Message Box",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "user clicks on Send Button",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "user closes the Message Box",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "user clicks on My Network icon",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "user clicks on Groups",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "user selects first group",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "user clicks Jobs Icon",
  "keyword": "Then "
});
formatter.match({
  "location": "MyNetworkPageSteps.user_is_on_MyNetwork_page()"
});
formatter.result({
  "duration": 133857400,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_on_more_invitations()"
});
formatter.result({
  "duration": 1284562000,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_on_see_all_connections()"
});
formatter.result({
  "duration": 1395558500,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_enters_name_on_search_text_box()"
});
formatter.result({
  "duration": 5303107600,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_message_button()"
});
formatter.result({
  "duration": 160365900,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_enters_text_on_Message_Box()"
});
formatter.result({
  "duration": 111570000,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_on_Send_Button()"
});
formatter.result({
  "duration": 2057469800,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_closes_the_Message_Box()"
});
formatter.result({
  "duration": 82921700,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_on_My_Network_icon()"
});
formatter.result({
  "duration": 51757800,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_on_Groups()"
});
formatter.result({
  "duration": 4247060800,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_selects_first_group()"
});
formatter.result({
  "duration": 2093050900,
  "status": "passed"
});
formatter.match({
  "location": "MyNetworkPageSteps.user_clicks_Jobs_Icon()"
});
formatter.result({
  "duration": 498846700,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Validate JobsPage Test",
  "description": "",
  "id": "linkedin-application-test;validate-jobspage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "user is on Jobs page",
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "user enters search jobs text box",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "user enters search location text box",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "user clicks search button",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "user adds data posted filter",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "user adds linkedin feature filter",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "user adds company filter",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "user adds experience level filter",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "user clears filter",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "user clicks messaging icon",
  "keyword": "Then "
});
formatter.match({
  "location": "JobsPageSteps.user_is_on_Jobs_page()"
});
formatter.result({
  "duration": 1014571500,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_enters_search_jobs_text_box()"
});
formatter.result({
  "duration": 1695417500,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_enters_search_location_text_box()"
});
formatter.result({
  "duration": 1288343600,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_clicks_search_button()"
});
formatter.result({
  "duration": 1124046300,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_adds_data_posted_filter()"
});
formatter.result({
  "duration": 3866127800,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_adds_linkedin_feature_filter()"
});
formatter.result({
  "duration": 3294213100,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_adds_company_filter()"
});
formatter.result({
  "duration": 4094079600,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_adds_experience_level_filter()"
});
formatter.result({
  "duration": 6319075400,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_clears_filter()"
});
formatter.result({
  "duration": 2326959300,
  "status": "passed"
});
formatter.match({
  "location": "JobsPageSteps.user_clicks_messaging_icon()"
});
formatter.result({
  "duration": 110941300,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Validate MessagingPage Test",
  "description": "",
  "id": "linkedin-application-test;validate-messagingpage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 39,
  "name": "user is on Messaging Page",
  "keyword": "Given "
});
formatter.step({
  "line": 40,
  "name": "user enters text message",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "user sends message",
  "keyword": "Then "
});
formatter.step({
  "line": 42,
  "name": "user clicks on notification page",
  "keyword": "Then "
});
formatter.match({
  "location": "MessagePageSteps.user_is_on_Messaging_Page()"
});
formatter.result({
  "duration": 1646361900,
  "status": "passed"
});
formatter.match({
  "location": "MessagePageSteps.user_enters_text_message()"
});
formatter.result({
  "duration": 2184342800,
  "status": "passed"
});
formatter.match({
  "location": "MessagePageSteps.user_sends_message()"
});
formatter.result({
  "duration": 2135265900,
  "status": "passed"
});
formatter.match({
  "location": "MessagePageSteps.user_clicks_on_notification_page()"
});
formatter.result({
  "duration": 128334200,
  "status": "passed"
});
formatter.scenario({
  "line": 44,
  "name": "Validate NotificationPage Test",
  "description": "",
  "id": "linkedin-application-test;validate-notificationpage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 45,
  "name": "user is on Notification Page",
  "keyword": "Given "
});
formatter.step({
  "line": 46,
  "name": "user clicks on profile page",
  "keyword": "Then "
});
formatter.match({
  "location": "NotificationPageSteps.user_is_on_Notification_Page()"
});
formatter.result({
  "duration": 3026396100,
  "status": "passed"
});
formatter.match({
  "location": "NotificationPageSteps.user_clicks_on_profile_page()"
});
formatter.result({
  "duration": 2317599700,
  "status": "passed"
});
formatter.scenario({
  "line": 48,
  "name": "Validate ProfilePage Test",
  "description": "",
  "id": "linkedin-application-test;validate-profilepage-test",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 49,
  "name": "user is on Profile Page",
  "keyword": "Given "
});
formatter.step({
  "line": 50,
  "name": "user clicks on profile page icon",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "user clicks on sign out",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "user closes browser",
  "keyword": "Then "
});
formatter.match({
  "location": "ProfilePageSteps.user_is_on_Notification_Page()"
});
formatter.result({
  "duration": 3017483100,
  "status": "passed"
});
formatter.match({
  "location": "ProfilePageSteps.user_clicks_on_profile_page_icon()"
});
formatter.result({
  "duration": 1056394900,
  "status": "passed"
});
formatter.match({
  "location": "ProfilePageSteps.user_clicks_on_sign_out()"
});
formatter.result({
  "duration": 1785359500,
  "status": "passed"
});
formatter.match({
  "location": "ProfilePageSteps.user_closes_browser()"
});
formatter.result({
  "duration": 1005991400,
  "status": "passed"
});
});