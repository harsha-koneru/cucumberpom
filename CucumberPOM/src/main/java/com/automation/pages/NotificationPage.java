package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class NotificationPage extends TestBase{
	
	@FindBy(id="nav-settings__dropdown-trigger")
	WebElement naviagtionSettings;
	
	@FindBy(xpath="//span[text() = 'View profile']/ancestor::a")
	WebElement viewProfile;
	
	public NotificationPage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateNotificationPageTitle(){
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver.getTitle();
	}
	
	public ProfilePage naviagtionSettings(){	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", naviagtionSettings);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		js.executeScript("arguments[0].click();", viewProfile);
		
		return new ProfilePage();	
	}
}
