package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class NetworkPage extends TestBase {
	
	// Web Elements
	
	@FindBy(id = "mynetwork-tab-icon")
	WebElement myNetworkIcon;
	
	@FindBy(xpath="//a[@href = '/mynetwork/invitation-manager/']")
	WebElement manageAll;
			
	@FindBy(xpath="//a[@href = '/mynetwork/invite-connect/connections/'][2]")
	WebElement seeAllConnections;
	
	@FindBy(xpath="//input[@placeholder = 'Search by name']")
	WebElement searchByName;
	
	@FindBy(xpath="//span[text() = 'Message']/parent::button")
	WebElement messageButton;
	
	@FindBy(xpath="//div[@role = 'textbox']")
	WebElement textMessage;
			
	@FindBy(xpath="//button[text() = 'Send']")
	WebElement sendButton;
	
	@FindBy(xpath="(//li-icon[@type = 'cancel-icon'])[2]")
	WebElement cancelButton;
	
	@FindBy(xpath="//a[@href = '/groups/my-groups/']")
	WebElement groups;
	
	@FindBy(xpath="//a[@href = '/groups/4332669/']")
	WebElement firstGroup;
	
	@FindBy(id = "jobs-tab-icon")
	WebElement jobsIcon;

	public NetworkPage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateNetworkPageTitle(){
		return driver.getTitle();
	}
	
	public void myNetworkIcon(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", myNetworkIcon);
	}
	
	public void manageAllLink(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", manageAll);
	}
	
	public void seeAllConnectionsLink(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", seeAllConnections);
	}
	
	public void searchByNameTextBox(){
		String friendName = prop.getProperty("searchName");
		searchByName.sendKeys(friendName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void messageButton(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", messageButton);
	}
	
	public void textMessageBox(){
		String message = prop.getProperty("message");
		textMessage.sendKeys(message);
	}
	
	public void sendButton(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", sendButton);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cancelButton(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", cancelButton);
	}
	
	public void groupsLink(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", groups);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void firstGroupLink(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", firstGroup);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public JobsPage jobsIcon(){	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", jobsIcon);
		
		return new JobsPage();	
	}
	
}
