package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class ProfilePage extends TestBase {
	
	@FindBy(id="nav-settings__dropdown-trigger")
	WebElement naviagtionSettings;
			
	@FindBy(xpath="// a[text() = 'Sign out']")
	WebElement signOut;

	public ProfilePage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateProfilePageTitle(){
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver.getTitle();
	}
	
	public void naviagtionSettings(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", naviagtionSettings);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void signOut(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", signOut);
	}
	
	public void closeBrowser(){
		driver.quit();
	}
}
