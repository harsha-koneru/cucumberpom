package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class MessagingPage extends TestBase{
	
	@FindBy(xpath="//div[@role = 'textbox']")
	WebElement textMessage;
			
	@FindBy(xpath="//button[text() = 'Send']")
	WebElement sendButton;
	
	@FindBy(id="notifications-tab-icon")
	WebElement notificationIcon;
	
	public MessagingPage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateMessagingPageTitle(){
		return driver.getTitle();
	}
	
	public void textMessageBox(){
		String message = prop.getProperty("message");
		textMessage.sendKeys(message);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendButton(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", sendButton);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public NotificationPage notificationIcon(){	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", notificationIcon);
		
		return new NotificationPage();	
	}
}
