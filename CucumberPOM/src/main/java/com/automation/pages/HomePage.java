package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class HomePage extends TestBase {
	
	// Web Elements
	
		@FindBy(id = "feed-tab-icon")
		WebElement homeIcon;
		
		@FindBy(id = "mynetwork-tab-icon")
		WebElement myNetworkIcon;
		
		public HomePage(){
			// PageFactory is used to initialize elements of a Page class without having to use ‘FindElement’ or ‘FindElements’
			PageFactory.initElements(driver, this);
		}
		
		public String validateHomePageTitle(){
			return driver.getTitle();
		}
		
		public NetworkPage networkIcon(){	
			// JavaScriptExecutor is an Interface that helps to execute JavaScript through Selenium Webdriver
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", myNetworkIcon);
			
			return new NetworkPage();	
		}

}
