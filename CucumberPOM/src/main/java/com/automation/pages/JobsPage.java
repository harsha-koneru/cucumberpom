package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class JobsPage extends TestBase{
	
	// Web Elements
		
	@FindBy(xpath="// input[@placeholder='Search jobs']")
	WebElement searchJobs;
	
	@FindBy(xpath="// input[@placeholder='Search location']")
	WebElement searchLocation;
	
	@FindBy(xpath="// button[text()='Search']")
	WebElement searchButton;
	
	@FindBy(xpath="// span[text() = 'Date Posted']/ancestor::button")
	WebElement datePosted;
	
	@FindBy(xpath="(// div[@id = 'date-posted-facet-values']/descendant::li)[4]/input")
	WebElement anyTimeDatePosted;
	
	@FindBy(xpath="(//span[text() = 'Apply']/parent::button)[1]")
	WebElement datePostedApply;
	
	@FindBy(xpath="// span[text() = 'LinkedIn Features']/ancestor::button")
	WebElement linkedInFeatures;
	
	@FindBy(xpath="(// div[@id = 'linkedin-features-facet-values']/descendant::li)[3]/input")
	WebElement underTenApplicants;
	
	@FindBy(xpath="(//span[text() = 'Apply']/parent::button)[2]")
	WebElement linkedInFeaturesApply;
	
	@FindBy(xpath="// span[text() = 'Company']/ancestor::button")
	WebElement company;
	
	@FindBy(xpath="(// div[@id = 'company-facet-values']/descendant::li)[5]/input")
	WebElement getCompany;
	
	@FindBy(xpath="(//span[text() = 'Apply']/parent::button)[3]")
	WebElement companyApply;
	
	@FindBy(xpath="// span[text() = 'Experience Level']/ancestor::button")
	WebElement experienceLevel;
	
	@FindBy(xpath="(// div[@id = 'experience-level-facet-values']/descendant::li)[3]/input")
	WebElement associateLevel;
	
	@FindBy(xpath="(//span[text() = 'Apply']/parent::button)[4]")
	WebElement experienceLevelApply;
	
	@FindBy(xpath="// span[text() = 'Clear ']/parent::button")
	WebElement clearFilters;
	
	@FindBy(id = "messaging-tab-icon")
	WebElement messagingIcon;
		
	
	public JobsPage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateJobsPageTitle(){
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver.getTitle();
	}
	
	public void searchJobs(){
		String jobTitle = prop.getProperty("jobTitle");
		searchJobs.sendKeys(jobTitle);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchLocation(){
		String jobLocation = prop.getProperty("jobLocation");
		searchLocation.sendKeys(jobLocation);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchButton(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", searchButton);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void datePosted(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", datePosted);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void anyTimeDatePosted(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", anyTimeDatePosted);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void datePostedApply(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", datePostedApply);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void linkedInFeatures(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", linkedInFeatures);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void underTenApplicants(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", underTenApplicants);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void linkedInFeaturesApply(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", linkedInFeaturesApply);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void company(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", company);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getCompany(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", getCompany);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void companyApply(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", companyApply);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void experienceLevel(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", experienceLevel);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void associateLevel(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", associateLevel);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void experienceLevelApply(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", experienceLevelApply);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clearFilters(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", clearFilters);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public MessagingPage messagingIcon(){	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", messagingIcon);
		
		return new MessagingPage();	
	}

}
