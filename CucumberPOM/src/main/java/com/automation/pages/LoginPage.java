package com.automation.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.automation.util.TestBase;

public class LoginPage extends TestBase{
	
	// Web Elements
	
	@FindBy(xpath="//a[text()='Sign in']")
	WebElement signInMainBtn;
	
	@FindBy(id = "username")
	WebElement username;
	
	@FindBy(id = "password")
	WebElement password;
	
	@FindBy(xpath="//button[text()='Sign in']")
	WebElement signInBtn;
	
	// Initializing Page Objects
	
	public LoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	public String validateMainPageTitle(){
		return driver.getTitle();
	}
	
	public void goToLogin(){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", signInMainBtn);
	}
	
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
	
	public HomePage login(String user_username, String user_password){
		username.sendKeys(user_username);
		password.sendKeys(user_password);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", signInBtn);
		
		return new HomePage();	
	}
}
