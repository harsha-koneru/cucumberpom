package com.automation.stepDefinitions;

import com.automation.pages.NotificationPage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class NotificationPageSteps extends TestBase{

	NotificationPage notificationPage;
	
	@Given("^user is on Notification Page$")
	public void user_is_on_Notification_Page() {
		notificationPage = new NotificationPage();
		String title = notificationPage.validateNotificationPageTitle();
		Assert.assertTrue(title.contains("Notifications | LinkedIn"));   
	}

	@Then("^user clicks on profile page$")
	public void user_clicks_on_profile_page() {
		notificationPage.naviagtionSettings();   
	}

}
