package com.automation.stepDefinitions;

import com.automation.pages.MessagingPage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class MessagePageSteps extends TestBase {
	
	MessagingPage messagingPage;
	
	@Given("^user is on Messaging Page$")
	public void user_is_on_Messaging_Page() {
		messagingPage = new MessagingPage();
		String title = messagingPage.validateMessagingPageTitle();
		Assert.assertTrue(title.contains("LinkedIn")); 
	}

	@Then("^user enters text message$")
	public void user_enters_text_message() {
		messagingPage.textMessageBox();
	}

	@Then("^user sends message$")
	public void user_sends_message() {
		messagingPage.sendButton();
	}

	@Then("^user clicks on notification page$")
	public void user_clicks_on_notification_page() {
		messagingPage.notificationIcon();
	}
}
