package com.automation.stepDefinitions;

import com.automation.pages.JobsPage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class JobsPageSteps extends TestBase {
	JobsPage jobsPage;
	
	@Given("^user is on Jobs page$")
	public void user_is_on_Jobs_page() {
		jobsPage = new JobsPage();
		String title = jobsPage.validateJobsPageTitle();
		Assert.assertTrue(title.contains("Jobs | LinkedIn"));
	}

	@Then("^user enters search jobs text box$")
	public void user_enters_search_jobs_text_box() {
	    jobsPage.searchJobs();
	}

	@Then("^user enters search location text box$")
	public void user_enters_search_location_text_box() {
	    jobsPage.searchLocation();   
	}

	@Then("^user clicks search button$")
	public void user_clicks_search_button() {
	    jobsPage.searchButton();
	}

	@Then("^user adds data posted filter$")
	public void user_adds_data_posted_filter() {
	    jobsPage.datePosted();
	    jobsPage.anyTimeDatePosted();
	    jobsPage.datePostedApply();  
	}

	@Then("^user adds linkedin feature filter$")
	public void user_adds_linkedin_feature_filter() {
	    jobsPage.linkedInFeatures();
	    jobsPage.underTenApplicants();
	    jobsPage.linkedInFeaturesApply();
	}

	@Then("^user adds company filter$")
	public void user_adds_company_filter() {
		jobsPage.company();
	    jobsPage.getCompany();
	    jobsPage.companyApply();  
	}

	@Then("^user adds experience level filter$")
	public void user_adds_experience_level_filter() {
		jobsPage.experienceLevel();
	    jobsPage.associateLevel();
	    jobsPage.experienceLevelApply(); 
	}

	@Then("^user clears filter$")
	public void user_clears_filter() {
		jobsPage.clearFilters();
	}

	@Then("^user clicks messaging icon$")
	public void user_clicks_messaging_icon() {
	    jobsPage.messagingIcon();
	}
}
