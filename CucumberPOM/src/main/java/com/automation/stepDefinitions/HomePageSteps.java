package com.automation.stepDefinitions;

import com.automation.pages.HomePage;
import com.automation.pages.LoginPage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class HomePageSteps extends TestBase{
	
	LoginPage loginPage;
	HomePage homePage;
	
	@Given("^user opens browser$")
	public void user_opens_browser() {
	     TestBase.initialization();  
	}

	@Then("^user is on linkedin page$")
	public void user_is_on_linkedin_page() {
		loginPage  = new LoginPage();
		String title = loginPage.validateMainPageTitle();
		Assert.assertEquals("LinkedIn India: Log In or Sign Up" ,title);  
	}

	@Then("^user clicks on signIn button$")
	public void user_clicks_on_signIn_button() {
	     loginPage.goToLogin();
	}

	@Then("^user is on login page$")
	public void user_is_on_login_page() {
		String title = loginPage.validateMainPageTitle();
		Assert.assertEquals("LinkedIn Login, LinkedIn Sign in | LinkedIn" ,title); 
	}

	@Then("^user logs into application$")
	public void user_logs_into_applicationn() {
	    homePage = loginPage.login(prop.getProperty("emailId"), prop.getProperty("password"));   
	}

	@Then("^validate home page title$")
	public void validate_home_page_title() {
		String title = homePage.validateHomePageTitle(); 
		Assert.assertTrue(title.contains("LinkedIn"));    
	}
	
	@Then("^user clicks MyNetwork Icon$")
	public void user_clicks_MyNetwork_Icon() {
		homePage.networkIcon();
	}

}
