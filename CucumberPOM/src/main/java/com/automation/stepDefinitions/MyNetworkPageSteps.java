package com.automation.stepDefinitions;

import com.automation.pages.NetworkPage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class MyNetworkPageSteps extends TestBase {
	
	NetworkPage networkPage;
	
	@Given("^user is on MyNetwork page$")
	public void user_is_on_MyNetwork_page() {
		networkPage = new NetworkPage();
		String title = networkPage.validateNetworkPageTitle();
		Assert.assertTrue(title.contains("LinkedIn")); 
	}

	@Then("^user clicks on more invitations$")
	public void user_clicks_on_more_invitations() {
		networkPage.manageAllLink();
	}

	@Then("^user clicks on see all connections$")
	public void user_clicks_on_see_all_connections() {
		networkPage.seeAllConnectionsLink();
	}
	
	@Then("^user enters name on search text box$")
	public void user_enters_name_on_search_text_box() {
	    networkPage.searchByNameTextBox(); 
	}

	@Then("^user clicks message button$")
	public void user_clicks_message_button() {
	    networkPage.messageButton();
	}

	@Then("^user enters text on Message Box$")
	public void user_enters_text_on_Message_Box() {
	    networkPage.textMessageBox();
	}

	@Then("^user clicks on Send Button$")
	public void user_clicks_on_Send_Button() {
	    networkPage.sendButton();
	}

	@Then("^user closes the Message Box$")
	public void user_closes_the_Message_Box() {
	    networkPage.cancelButton();
	}

	@Then("^user clicks on My Network icon$")
	public void user_clicks_on_My_Network_icon() {
	    networkPage.myNetworkIcon(); 
	}

	@Then("^user clicks on Groups$")
	public void user_clicks_on_Groups() {
	    networkPage.groupsLink(); 
	}

	@Then("^user selects first group$")
	public void user_selects_first_group() {
	    networkPage.firstGroupLink(); 
	}

	@Then("^user clicks Jobs Icon$")
	public void user_clicks_Jobs_Icon() {
	    networkPage.jobsIcon(); 
	}

}
