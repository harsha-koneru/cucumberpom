package com.automation.stepDefinitions;

import com.automation.pages.ProfilePage;
import com.automation.util.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

public class ProfilePageSteps extends TestBase {
	
	ProfilePage profilePage;
	
	@Given("^user is on Profile Page$")
	public void user_is_on_Notification_Page() {
		profilePage = new ProfilePage();
		String title = profilePage.validateProfilePageTitle();
		Assert.assertTrue(title.contains("Harshavardhan Koneru | LinkedIn"));    
	}

	@Then("^user clicks on profile page icon$")
	public void user_clicks_on_profile_page_icon() {
		profilePage.naviagtionSettings();
	}

	@Then("^user clicks on sign out$")
	public void user_clicks_on_sign_out() {
		profilePage.signOut();
	}

	@Then("^user closes browser$")
	public void user_closes_browser() {
		profilePage.closeBrowser();
	}
}
