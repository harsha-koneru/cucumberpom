Feature: LinkedIn Application Test

Scenario: Validate HomePage Test
	Given user opens browser
	Then user is on linkedin page
	Then user clicks on signIn button
	Then user is on login page
	Then user logs into application
	Then validate home page title
	Then user clicks MyNetwork Icon
	
Scenario: Validate MyNetworkPage Test
	Given user is on MyNetwork page
	Then user clicks on more invitations
	Then user clicks on see all connections
	Then user enters name on search text box
	Then user clicks message button
	Then user enters text on Message Box
	Then user clicks on Send Button
	Then user closes the Message Box
	Then user clicks on My Network icon 
	Then user clicks on Groups
	Then user selects first group 
	Then user clicks Jobs Icon
	
Scenario: Validate JobsPage Test
	Given user is on Jobs page
	Then user enters search jobs text box
	Then user enters search location text box
	Then user clicks search button
	Then user adds data posted filter
	Then user adds linkedin feature filter
	Then user adds company filter
	Then user adds experience level filter
	Then user clears filter
	Then user clicks messaging icon
	
Scenario: Validate MessagingPage Test
	Given user is on Messaging Page
	Then user enters text message
	Then user sends message
	Then user clicks on notification page

Scenario: Validate NotificationPage Test
	Given user is on Notification Page
	Then user clicks on profile page

Scenario: Validate ProfilePage Test
	Given user is on Profile Page
	Then user clicks on profile page icon
	Then user clicks on sign out
	Then user closes browser 